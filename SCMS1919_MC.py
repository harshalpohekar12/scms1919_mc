import random
if __name__ == "__main__":
    # list of player names
    players = []

    # read player names from file
    with open(r"players.txt", "r") as file:
        players = file.readlines()

    # filter out player names
    playerNames = [player.replace("\n","") for player in players]

    # run the musical chair games for 5 times
    for i in range(5):
        # deep copy player names
        players = playerNames[:]

        # list to store discarded players
        discardedPlayers = []

        # run the game till all are discarded
        while len(players):
            discardedPlayer = random.choice(players)
            discardedPlayers.append(discardedPlayer)
            players.pop(players.index(discardedPlayer))

        #print the result
        print("List of players for run number %d" % (i + 1))
        for player in discardedPlayers:
            print(player)
        print("Winner of the round %d is %s.\n" % (i + 1, discardedPlayers[-1]))
        
        
    
